package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

type bag struct {
	B [5][5]string
	E string
	T string
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/lifegame", http.StatusFound)
	})
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
	})
	http.HandleFunc("/lifegame", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(os.Stdout, strings.Join([]string{r.RemoteAddr, "-", "-", "[" + time.Now().Format(time.RFC3339) + "]", "\"" + r.Method, r.RequestURI, r.Proto + "\""}, " "))
		var b [5][5]string
		prev := r.FormValue("prev")
		if prev != "" {
			rows := strings.Split(prev, "9")
			for i, row := range rows {
				cols := strings.Split(row, "")
				for j, col := range cols {
					if col == "0" {
						b[i][j] = "□"
					} else if col == "1" {
						b[i][j] = "■"
					}
				}
			}
			var bb [5][5]string
			for i := 0; i < 5; i++ {
				for j := 0; j < 5; j++ {
					bb[i][j] = "□"
				}
			}
			for i := 0; i < 5; i++ {
				for j := 0; j < 5; j++ {
					// 誕生の場合
					if b[i][j] == "□" {
						count := 0
						if i > 0 && j > 0 && b[i-1][j-1] == "■" {
							count++
						}
						if i > 0 && b[i-1][j] == "■" {
							count++
						}
						if i > 0 && j < 4 && b[i-1][j+1] == "■" {
							count++
						}
						if j > 0 && b[i][j-1] == "■" {
							count++
						}
						if j < 4 && b[i][j+1] == "■" {
							count++
						}
						if i < 4 && j > 0 && b[i+1][j-1] == "■" {
							count++
						}
						if i < 4 && b[i+1][j] == "■" {
							count++
						}
						if i < 4 && j < 4 && b[i+1][j+1] == "■" {
							count++
						}
						if count == 3 {
							bb[i][j] = "■"
						} else {
							bb[i][j] = "□"
						}
					} else {
						bb[i][j] = "□"
					}
					// 生存・過疎・過密の場合
					if b[i][j] == "■" {
						count := 0
						if i > 0 && b[i-1][j] == "■" {
							count++
						}
						if j > 0 && b[i][j-1] == "■" {
							count++
						}
						if j < 4 && b[i][j+1] == "■" {
							count++
						}
						if i < 4 && b[i+1][j] == "■" {
							count++
						}
						if count == 2 || count == 3 {
							bb[i][j] = "■"
						} else {
							bb[i][j] = "□"
						}
					}
				}
			}
			ss := make([]string, 0, 5)
			for _, row := range bb {
				c := make([]string, 0, 5)
				for _, col := range row {
					if col == "□" {
						c = append(c, "0")
					} else if col == "■" {
						c = append(c, "1")
					}
				}
				ss = append(ss, strings.Join(c, ""))
			}
			bag := new(bag)
			bag.B = bb
			bag.E = strings.Join(ss, "9")
			bag.T = fmt.Sprintf("%s", time.Now().Format(time.RFC3339))
			tmpl := template.Must(template.New("lifegame.tmpl").ParseFiles("views/lifegame.tmpl"))
			tmpl.Execute(w, bag)
		} else {
			for i := 0; i < 5; i++ {
				for j := 0; j < 5; j++ {
					b[i][j] = "□"
				}
			}
			b[2][1] = "■"
			b[2][2] = "■"
			b[2][3] = "■"
			ss := make([]string, 0, 5)
			for _, row := range b {
				c := make([]string, 0, 5)
				for _, col := range row {
					if col == "□" {
						c = append(c, "0")
					} else if col == "■" {
						c = append(c, "1")
					}
				}
				ss = append(ss, strings.Join(c, ""))
			}
			bag := new(bag)
			bag.B = b
			bag.E = strings.Join(ss, "9")
			bag.T = fmt.Sprintf("%s", time.Now().Format(time.RFC3339))
			tmpl := template.Must(template.New("lifegame.tmpl").ParseFiles("views/lifegame.tmpl"))
			tmpl.Execute(w, bag)
		}
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
