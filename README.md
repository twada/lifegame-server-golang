legacy-lifegame-golang
=======================================

Golang project for "Refactoring Legacy Code" workshop


SETUP
---------------------------------------

### with docker & docker-compose

```sh
$ docker-compose build
$ docker-compose up
```

entering CLI mode

```sh
$ docker-compose run --rm web bash
```

### local environment

```sh
$ go run main.go
```


DEMO
---------------------------------------

http://localhost:8080/lifegame
